require 'test_helper'

class DrawControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get draw_create_url
    assert_response :success
  end

  test "should get show" do
    get draw_show_url
    assert_response :success
  end

  test "should get index" do
    get draw_index_url
    assert_response :success
  end

end
