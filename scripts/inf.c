// author: Kyohei Shimozato
// description: cnn inference program
// Usage Example: ./inf data.dat 8.bmp

#include "nn.h"
#include <time.h>

typedef struct {
  float w[5*5*1*20];
  float b[20];
} Conv1;
typedef struct {
  float w[5*5*20*50];
  float b[50];
} Conv2;
typedef struct {
  float A[4*4*50*500];
  float b[500];
} Fc1;
typedef struct {
  float A[500*10];
  float b[10];
} Fc2;
typedef struct {
  Conv1 conv1;
  Conv2 conv2;
  Fc1 fc1;
  Fc2 fc2;
} P;
typedef struct {
  float conv1[28*28];
  float pool1[24*24*20];
  float relu1[12*12*20];
  float conv2[12*12*20];
  float pool2[8*8*50];
  float relu2[4*4*50];
  float fc1[4*4*50];
  float relu3[500];
  float fc2[500];
  float soft[10];
  float y[10];
} X;
#define for_all(func,p) do{\
  func(5*5*1*20,    p->conv1.w);\
  func(5*5*20*50,   p->conv2.w);\
  func(20,          p->conv1.b);\
  func(50,          p->conv2.b);\
  func(4*4*50*500,  p->fc1.A);\
  func(500,         p->fc1.b);\
  func(10*500,      p->fc2.A);\
  func(10,          p->fc2.b);\
}while(0)
void copy(int n,const float *x,float *y){
  int i; for(i=0;i<n;i++){
    y[i]=x[i];
  }
}
void fc(int m,int n,const float *x,const float *A,const float *b,float *y){
  //y=Ax+b
  int i,j;
  for(i=0;i<m;i++){
    y[i]=b[i];
    for(j=0;j<n;j++){
      y[i]+=A[i*n+j]*x[j];
    }
  }
}
void relu(int n,const float *x,float *y){
  //remove x<=0
  int i;
  for(i=0;i<n;i++){
    if(x[i]>0){
      y[i]=x[i];
    }else{
      y[i]=0;
    }
  }
}
void pool(int n,int c,const float *x,float *y){
  int i,j,k,l,s;
  for(s=0;s<c;s++){
    for(i=0;i<n/2;i++){
      for(j=0;j<n/2;j++){
        y[(n/2)*(n/2)*s+i*n/2+j]=x[n*n*s+i*n*2+j*2];
        for(k=0;k<2;k++){
          for(l=0;l<2;l++){
            if(y[(n/2)*(n/2)*s+i*n/2+j]<x[n*n*s+(2*i+k)*n+(2*j+l)]){
              y[(n/2)*(n/2)*s+i*n/2+j]=x[n*n*s+(2*i+k)*n+(2*j+l)];
            }
          }
        }
      }
    }
  }
}
void conv(int n,int m,int c,int o,const float *x,const float *w,const float *b,float *y){
  //n:input image size
  //m:filter size
  //c:channel number
  //o:filter number
  //x:n*n*c input image
  //w:m*m*o filter
  //b:(n-m+1)*(n-m+1)*o bias
  //y:(n-m+1)*(n-m+1)*o output

  int i,j,k,l,s,t;
  copy((n-m+1)*(n-m+1)*o,b,y);
  for(l=0;l<o;l++){
    for(i=0;i<(n-m+1);i++){
      for(j=0;j<(n-m+1);j++){
        y[(n-m+1)*(n-m+1)*l+i*(n-m+1)+j]=b[l];
        for(k=0;k<c;k++){
          for(s=0;s<m;s++){
            for(t=0;t<m;t++){
              y[(n-m+1)*(n-m+1)*l+i*(n-m+1)+j]+=
                w[m*m*(l*c+k)+s*m+t]*x[n*n*k+((i+s)*n+(j+t))];
            }
          }
        }
      }
    }
  }
}
void softmax(int n,const float *x,float *y){
  int i;
  float x_max=0.0;
  float exp_sum=0.0;

  for(i=0;i<n;i++){
    x_max=x[i]>x_max?x[i]:x_max;
  }
  for(i=0;i<n;i++){
    exp_sum+=exp(x[i]-x_max);
  }
  for(i=0;i<n;i++){
    y[i]=exp(x[i]-x_max)/exp_sum;
  }
}
int inference(const P *p,const float *x_input,float *y){
  int i,ans=-1;
  float max=0;
  X x;

  copy(28*28,x_input,x.conv1);

  conv(28,5,1,20,x.conv1,p->conv1.w,p->conv1.b,x.pool1);
  pool(24,20,x.pool1,x.relu1);
  relu(12*12*20,x.relu1,x.conv2);

  conv(12,5,20,50,x.conv2,p->conv2.w,p->conv2.b,x.pool2);
  pool(8,50,x.pool2,x.relu2);

  relu(4*4*50,x.relu2,x.fc1);

  fc(500,4*4*50,x.fc1,p->fc1.A,p->fc1.b,x.relu3);
  relu(500,x.relu3,x.fc2);

  fc(10,500,x.fc2,p->fc2.A,p->fc2.b,x.soft);
  softmax(10,x.soft,x.y);

  for(i=0;i<10;i++){
    if(x.y[i]>max){
      max=x.y[i];
      ans=i;
    }
  }
  copy(10,x.y,y);
  if(ans==-1){
    puts("Invalid answer");
    exit(1);
  }
  return ans;
}
//Don't forget to define *file before use load_param
#define load_param(size,param) do{\
  fread(param,sizeof(float),size,file);\
}while(0)
void load(const char *filename,P *p){
  FILE *file=fopen(filename,"r");
  if(file==NULL){
    printf("Load Error: %s\n",filename);
    exit(1);
  }else{
    for_all(load_param,p);
    fclose(file);
    //printf("Loaded: %s\n",filename);
  }
}
int main(int argc,char *argv[]) {

  if(argc!=3){
    puts("Error: Wrong args!");
    return 1;
  }

  P *p       =(P *)malloc(sizeof(P));
  float y[10];
  float *x=load_mnist_bmp(argv[2]);

  load(argv[1],p);

  printf("%d\n",inference(p,x,y));

  
  return 0;
}
