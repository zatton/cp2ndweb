$(document).on 'turbolinks:load', ->
  try
    $(window).unbind()
    $body = $("body")
    controller_camel = $body.data("controller").replace /[-_](.)/g, (match, c) ->
      return c.toUpperCase()
    controller=controller_camel.charAt(0).toUpperCase() + controller_camel.slice(1)
    action = $body.data("action")
    if window[controller]
      activeController = new window[controller]
      if activeController && $.isFunction(activeController[action])
        activeController[action]()

    if window.ga != undefined
      ga('set', 'location', location.href.split('#')[0])
      ga('send', 'pageview')
