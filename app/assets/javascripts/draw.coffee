class @Draw
  create: ->
    console.log 'draw#create'
    canvas = $('#draw')
    ctx = canvas[0].getContext('2d')
    ctx.lineWidth = 5
    ctx.lineJoin = "round"
    ctx.lineCap = "round"

    ctx.fillStyle = "white";
    ctx.fillRect(0, 0, canvas[0].width, canvas[0].height)

    drawing = false
    thinking = 0

    getPosition = (e)->
      console.log e
      position = {}
      if(e.type in ['touchstart','touchmove','touchend'])
        touches = e.originalEvent
        position = {x:touches.layerX, y:touches.layerY}
      else
        position = {x:e.offsetX,y:e.offsetY}
      position


    canvas.on 'mousedown touchstart' ,(e)->
      e.preventDefault()
      position = getPosition(e)
      console.log 'mouseDown'
      console.log position
      drawing = true

      ctx.beginPath()
      ctx.moveTo(position.x,position.y)
    canvas.on 'mousemove touchmove' ,(e)->
      e.preventDefault()
      return unless drawing
      position = getPosition(e)
      console.log 'mouseMove'
      console.log position
      ctx.lineTo(position.x,position.y)
      ctx.stroke()
      ctx.moveTo(position.x,position.y)
    canvas.on 'mouseup mouseout touchend' ,(e)->
      return unless drawing
      drawing = false
      ctx.closePath()
      console.log 'closePath'
      e.preventDefault()

      url = canvas[0].toDataURL()
      $('#answer').text('Thinking...')
      thinking++
      $.ajax
        url: '/inference'
        data:
          image: url
          authenticity_token: $('meta[name="csrf-token"]').attr('content')
        cache: false
        type: 'POST'
      .done (data)->
        thinking--
        if thinking == 0
          $('#answer').text "answer: #{data.answer}"

    $('#clear').on 'click', ->
      ctx.fillRect(0, 0, canvas[0].width, canvas[0].height)
      $('#answer').text('Draw a number')
