class DrawController < ApplicationController
  require 'base64'
  require 'open3'
  def create
  end

  def show
  end

  def index
  end

  def inference
    image =params[:image].gsub(/(data:image\/png;base64,)/,'').encode('ASCII-8BIT')
    File.open("./scripts/img.png",'wb') do |file|
      logger.info file.path
      file.write(Base64.urlsafe_decode64(image))
    end
    %x[convert -geometry 28x28 -negate -normalize scripts/img.png scripts/img.bmp]
    cmd= %x[ ./scripts/inf scripts/data.dat scripts/img.bmp ]
    render json: {answer: cmd}
  end
end
