Rails.application.routes.draw do
  root to: 'draw#create'

  get 'draw/create'

  get 'draw/show'

  get 'draw/index'

  post 'inference' => 'draw#inference'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
